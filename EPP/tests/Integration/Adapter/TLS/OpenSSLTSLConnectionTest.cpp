#include <string>
#include <memory>
#include <gtest/gtest.h>
#include "Poco/Exception.h"
#include "Poco/DateTime.h"
#include "Poco/DateTimeFormat.h"
#include "Poco/DOM/NodeList.h"
#include "Poco/DOM/Document.h"
#include "Poco/DOM/DOMParser.h"
#include "Poco/DateTimeFormatter.h"
#include "EPP/EPPConnectionManager.h"
#include "EPP/EPPConnectionAdapter.h"
#include "EPP/EPPConnectionInterface.h"
#include "EPP/EPPConnectionParameters.h"

class OpenSSLTSLConnectionTest : public ::testing::Test
{
protected:
    void SetUp() override
    {
        rootCertificate.append("SSL/root.pem");
        clientCertificate.append("SSL/client.pem");

        connectionParameters.serverPort         = 700;
        connectionParameters.rootCertificate    = rootCertificate;
        connectionParameters.clientCertificate  = clientCertificate;
        connectionParameters.serverAddress      = "beta.registro.br";
        connectionParameters.passphrase         = "0659c7992e268962384eb17fafe88364";
    }

    std::string clientCertificate = TESTS_RESOURCES_PATH;
    std::string rootCertificate   = TESTS_RESOURCES_PATH;

    Poco::XML::DOMParser parser;
    EPP::EPPConnectionParameters connectionParameters;

};

TEST_F(OpenSSLTSLConnectionTest, ReadWriteOnSSLStreaming) {

    std::string expected_svID               = "Registro.br EPP Server";
    std::string expected_version            = "1.0";
    std::string expected_lang_pt            = "pt";
    std::string expected_lang_en            = "en";
    std::string expected_contact_objectURI  = "urn:ietf:params:xml:ns:contact-1.0";
    std::string expected_domain_objectURI   = "urn:ietf:params:xml:ns:domain-1.0";
    std::string expected_domain_extURI      = "urn:ietf:params:xml:ns:brdomain-1.0";
    std::string expected_org_extURI         = "urn:ietf:params:xml:ns:brorg-1.0";

    std::unique_ptr<EPP::EPPConnectionInterface> eppConnection = nullptr;
    ASSERT_NO_THROW(eppConnection = EPP::EPPConnectionManager::connect(EPP::RAW_OPENSSL, connectionParameters));

    try {
        auto response = eppConnection->receive();
        Poco::XML::Document * document = parser.parseString(response);

        ASSERT_FALSE(response.empty());

        Poco::XML::NodeList * svID = document->getElementsByTagName("svID");
        ASSERT_EQ(expected_svID, svID->item(0)->innerText());

        Poco::XML::NodeList * svDate = document->getElementsByTagName("svDate");
        auto clientTimeNow = Poco::DateTimeFormatter::format(Poco::DateTime(), Poco::DateTimeFormat::ISO8601_FORMAT).substr(0, 16);
        auto serverTimeNow = svDate->item(0)->innerText().substr(0, 16);
        ASSERT_EQ(clientTimeNow, serverTimeNow);

        Poco::XML::NodeList * version = document->getElementsByTagName("version");
        ASSERT_EQ(expected_version, version->item(0)->innerText());

        Poco::XML::NodeList * languages = document->getElementsByTagName("lang");
        ASSERT_EQ(expected_lang_en, languages->item(0)->innerText());
        ASSERT_EQ(expected_lang_pt, languages->item(1)->innerText());

        Poco::XML::NodeList * objURI = document->getElementsByTagName("objURI");
        ASSERT_EQ(expected_domain_objectURI, objURI->item(0)->innerText());
        ASSERT_EQ(expected_contact_objectURI, objURI->item(1)->innerText());

        Poco::XML::NodeList * extURI = document->getElementsByTagName("extURI");
        ASSERT_EQ(expected_domain_extURI, extURI->item(0)->innerText());
        ASSERT_EQ(expected_org_extURI, extURI->item(1)->innerText());

    } catch (Poco::Exception & exception) {
        std::cout << exception.message() << std::endl;
    }

    ASSERT_NO_THROW(eppConnection->disconnect());

}


