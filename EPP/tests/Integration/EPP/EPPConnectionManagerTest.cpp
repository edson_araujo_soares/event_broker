#include <string>
#include <memory>
#include <gtest/gtest.h>
#include "EPP/EPPConnectionManager.h"
#include "EPP/EPPConnectionAdapter.h"
#include "EPP/EPPConnectionInterface.h"
#include "EPP/EPPConnectionParameters.h"

class EPPConnectionManagerTest : public ::testing::Test
{
protected:
    void SetUp() override
    {
        rootCertificate.append("SSL/root.pem");
        clientCertificate.append("SSL/client.pem");

        connectionParameters.serverPort         = 700;
        connectionParameters.rootCertificate    = rootCertificate;
        connectionParameters.clientCertificate  = clientCertificate;
        connectionParameters.serverAddress      = "beta.registro.br";
        connectionParameters.passphrase         = "0659c7992e268962384eb17fafe88364";
    }

    EPP::EPPConnectionParameters connectionParameters;
    std::string clientCertificate = TESTS_RESOURCES_PATH;
    std::string rootCertificate   = TESTS_RESOURCES_PATH;
    std::shared_ptr<EPP::EPPConnectionInterface> eppConnection = nullptr;

};

TEST_F(EPPConnectionManagerTest, ConnectUsingOpenSSLStrategy) {

    ASSERT_NO_THROW(eppConnection = EPP::EPPConnectionManager::connect(EPP::RAW_OPENSSL, connectionParameters));
    ASSERT_NO_THROW(eppConnection->disconnect());

}

TEST_F(EPPConnectionManagerTest, ConnectUsingBoostStrategy) {

    ASSERT_NO_THROW(eppConnection = EPP::EPPConnectionManager::connect(EPP::TLS_WITH_BOOST, connectionParameters));
    ASSERT_NO_THROW(eppConnection->disconnect());

}
