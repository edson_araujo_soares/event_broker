#include <fcntl.h>
#include <cstring>
#include <unistd.h>
#include <openssl/err.h>
#include <openssl/rand.h>
// #include <sys/select.h>
// #include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include "Poco/Exception.h"
#include "boost/locale.hpp"
#include "Adapter/TLS/OpenSSLTLSConnection.h"

namespace TLS {


    OpenSSLTLSConnection::OpenSSLTLSConnection(const EPP::EPPConnectionParameters & parameters)
        : _sslObject(nullptr),
          _sslContext(nullptr),
          _connectionSocket(nullptr),
          certificateCommonNameCheckingEnabled(false)
    {
        init(parameters.rootCertificate, parameters.clientCertificate, parameters.passphrase);

        // BIO_new_connect
        // Return value: creates a new connect BIO
        std::string connectionString = parameters.serverAddress + ":" + std::to_string(parameters.serverPort);
        _connectionSocket = BIO_new_connect((char*) connectionString.c_str());

        // BIO_do_connect
        // Return value: 1 or x
        //               1      (if the connection was established successfully).
        //               x <= 0 (if the connection could not be established)
        if ( BIO_do_connect(_connectionSocket) != 1 )
            throw Poco::Exception("Connection could not be established", openSSLMessage(), SSLErrorCode::BIO_DO_CONNECT_ERR);

        // Create a new SSL structure for a connection
        if ( !(_sslObject = SSL_new(_sslContext)) )
            throw Poco::Exception("Error creating an SSL context", openSSLMessage(), SSLErrorCode::SSL_NEW_ERR);

        // Connect the SSL object with BIO and initiate the TLS handshake
        // with the TLS server
        SSL_set_bio(_sslObject, _connectionSocket, _connectionSocket);

        nonBlockingIODescriptor();
        int connectStatus = SSL_connect(_sslObject);
        while ( connectStatus == -1 ) {
            int connectError = SSL_get_error(_sslObject, connectStatus);
            struct timeval timeout;
            timeout.tv_sec = CONNECTION_TIMEOUT;
            timeout.tv_usec = 0;

            int fd = SSL_get_fd(_sslObject);
            fd_set fdset;
            FD_ZERO(&fdset);
            FD_SET(fd, &fdset);
            int hasData = 0;

            if ( connectError == SSL_ERROR_WANT_READ )
                hasData = select(fd + 1, &fdset, nullptr, nullptr, &timeout);
            else if ( connectError == SSL_ERROR_WANT_WRITE )
                hasData = select(fd + 1, nullptr, &fdset, nullptr, &timeout);

            if ( hasData <= 0 )
                throw Poco::Exception("Error connecting SSL object", openSSLMessage(), SSLErrorCode::SSL_CONNECT_ERR);

            connectStatus = SSL_connect(_sslObject);
        }

        if ( connectStatus <= 0 )
            throw Poco::Exception("Error connecting SSL object", openSSLMessage(), SSLErrorCode::SSL_CONNECT_ERR);

        if ( certificateCommonNameCheckingEnabled && !isCertificateCommonNameOk(parameters.serverAddress) ) {
            disconnect();
            throw Poco::Exception(
                "Peer's certificate common name mismatch",
                 X509_verify_cert_error_string(X509_V_ERR_APPLICATION_VERIFICATION),
                 SSLErrorCode::PEER_CERTIFICATE_CN_ERR
             );
        }
    }

    OpenSSLTLSConnection::~OpenSSLTLSConnection()
    {
        if (_sslObject)
            SSL_free(_sslObject);

        if (_sslContext)
            SSL_CTX_free(_sslContext);

        BIO_free (_connectionSocket);
        ERR_free_strings ();
    }

    int OpenSSLTLSConnection::pem_passwd_cb(char *buf, int size, int rwflag, void *userdata)
    {
        std::strncpy(buf, (char*)_passphrase.c_str(), size);
        buf[size-1] = '\0';
        return strlen(buf);
    }

    std::string OpenSSLTLSConnection::openSSLMessage() const
    {
        const int MSG_SIZE = 200;
        char error_msg[MSG_SIZE];
        unsigned long e = ERR_get_error();
        ERR_error_string(e, error_msg);
        std::string result =  error_msg;

        return result;
    }

    void OpenSSLTLSConnection::seed_prng()
    {
        // "What happens is that when too much randomness is drawn from the
        // operating system's randomness pool then randomness can
        // temporarily be unavailable./dev/random solves this problem by
        // waiting until enough randomness can be gathered - and this can
        // take a long time since blocking reduces activity in the machine
        // and less activity provides less random events: a vicious
        // circle. /dev/urandom solves this dilemma more pragmatically by
        // simply returning predictable ``random'' numbers."
        //
        // lists.fifi.org/cgi-bin/man2html/usr/share/man/man3/Net::SSLeay.3pm.gz

#ifdef HAVE_SRANDOMDEV
        RAND_load_file("/dev/random", 2048);
#else
        RAND_load_file("/dev/urandom", 2048);
#endif //HAVE_SRANDOMDEV
    }

    void OpenSSLTLSConnection::disconnect()
    {
        int fd = -1;
        if ( _sslObject != nullptr )
            fd = SSL_get_fd(_sslObject);

        if ( fd >= 0 ) {
            int r = SSL_shutdown(_sslObject);
            // If we called SSL_shutdown() first then we always get return
            // value of '0'. In this case, try again, but first send a TCP FIN
            // to trigger the other side's close_notify
            if ( r == 0 ) {
                shutdown(fd, 1);
                SSL_shutdown(_sslObject);
            }
            SSL_free(_sslObject);
            if ( _sslContext )
                SSL_CTX_free(_sslContext);

            close(fd);
            _sslObject        = nullptr;
            _sslContext       = nullptr;
            _connectionSocket = nullptr;
        }
    }

    std::string OpenSSLTLSConnection::receive()
    {
        struct timeval to;
        to.tv_sec = WRITE_AND_READ_TIMEOUT;
        to.tv_usec = 0;
        int has_data;
        fd_set readfds, writefds;
        bool read_blocked = false;
        bool read_blocked_on_write = false;
        bool done = false;
        uint32_t to_read;

        const uint32_t BUF_SZ = 2049;
        const uint32_t TOTAL_LENGTH_SZ = 4;
        unsigned char buf[BUF_SZ];
        uint32_t total_length = 0;
        uint32_t bytes_read = 0;
        std::string xml_payload;

        // checks connection
        if ( _connectionSocket == nullptr )
            throw Poco::Exception("Lost connection to peer", openSSLMessage(), SSLErrorCode::LOST_CONNECTION);

        int fd = SSL_get_fd(_sslObject);
        while (!done) {
            FD_ZERO(&readfds);
            FD_ZERO(&writefds);
            FD_SET(fd, &readfds);
            if (read_blocked_on_write) {
                // Read the comments on SSL_ERROR_WANT_WRITE below
                FD_SET(fd, &writefds);
                has_data = select(fd+1, &readfds, &writefds, nullptr, &to);
            } else {
                // Wait for data on the network read buffer
                has_data = select(fd+1, &readfds, nullptr, nullptr, &to);
            }
            if (has_data == 0) {
                if (total_length == 0 || (total_length > 0 && xml_payload == "")) // xml_payload replaced payload argument
                    throw Poco::Exception("Read Operation Timeout", SSLErrorCode::READ_TIMEOUT);
                else
                    throw Poco::Exception("Incomplete XML payload", SSLErrorCode::PAYLOAD_INCOMPLETE);

            } else if (has_data < 0) {
                throw Poco::Exception("Could not read the data unit", SSLErrorCode::SSL_READ_ERR);
            }

            if (FD_ISSET(fd, &readfds) || (read_blocked_on_write && FD_ISSET(fd, &writefds))) {
                do {
                    read_blocked_on_write = false;
                    read_blocked = false;

                    int r;
                    if (total_length == 0) {
                        to_read = TOTAL_LENGTH_SZ - bytes_read;
                        r = SSL_read(_sslObject, buf + bytes_read, to_read);
                    } else {
                        memset(buf, 0, BUF_SZ);
                        to_read = total_length - bytes_read;
                        if (to_read > (BUF_SZ - 1)) {
                            to_read = BUF_SZ-1;
                        }
                        r = SSL_read(_sslObject, buf, to_read);
                    }

                    switch (SSL_get_error(_sslObject, r)) {
                        case SSL_ERROR_NONE:
                            bytes_read += r;
                            if (total_length == 0) {
                                if (bytes_read < TOTAL_LENGTH_SZ) {
                                    // Still haven't read the first 4 bytes
                                    break;
                                }

                                memcpy(&total_length, buf, TOTAL_LENGTH_SZ);
                                total_length = ntohl(total_length);
                                if (total_length <= TOTAL_LENGTH_SZ) { // No payload to be read
                                    throw Poco::Exception("Invalid total length of the data unit", SSLErrorCode::INVALID_TOTAL_LENGTH);
                                }
                            } else {
                                xml_payload += (char *)buf;
                            }

                            if (bytes_read == total_length) {
                                done = true;
                            }
                            break;
                        case SSL_ERROR_WANT_READ:
                            // This happens when the SSL record arrives in two (or more)
                            // pieces. The first piece makes the select() call signal
                            // there is data in the network buffer, but the SSL buffer
                            // is still empty. Another select() call is needed.
                            read_blocked = true;
                            break;
                        case SSL_ERROR_WANT_WRITE:
                            // This happens when we're trying to rehandshake and we
                            // block on a write during that rehandshake. Another
                            // select() call for waiting the socket to be writable is
                            // needed. As soon as it is, reinitiate the read.
                            read_blocked_on_write = true;
                            break;
                        case SSL_ERROR_ZERO_RETURN:
                            // shutdown and exception
                            SSL_shutdown(_sslObject);
                        default:
                            throw Poco::Exception("Could not read the data unit", openSSLMessage(), SSLErrorCode::SSL_READ_ERR);
                    }
                } while (SSL_pending(_sslObject) && !read_blocked && !read_blocked_on_write && !done);
            }
        }

        return xml_payload;
    }

    void OpenSSLTLSConnection::send(const std::string & payload)
    {
        struct timeval to;
        to.tv_sec = WRITE_AND_READ_TIMEOUT;
        to.tv_usec = 0;

        int has_data;
        fd_set readfds, writefds;
        bool done = false;
        bool write_blocked_on_read = false;

        std::string utf8_xml_payload = boost::locale::conv::to_utf<char>(payload,"Latin1");

        const uint32_t TOTAL_LENGTH_SZ = 4;
        const uint32_t PAYLOAD_SZ = (uint32_t) utf8_xml_payload.length();
        const uint32_t TOTAL_LENGTH = TOTAL_LENGTH_SZ + PAYLOAD_SZ;

        unsigned char *data_unit = new unsigned char[TOTAL_LENGTH + 1];

        memset(data_unit, 0, TOTAL_LENGTH + 1);

        // TOTAL LENGTH
        uint32_t wf_total_length = htonl(TOTAL_LENGTH);
        memcpy(data_unit, &wf_total_length, TOTAL_LENGTH_SZ);

        // EPP XML INSTANCE
        memcpy(data_unit+4, (char *) utf8_xml_payload.c_str(), PAYLOAD_SZ);

        // WRITE OPERATION

        // checks connection
        if ( _connectionSocket == nullptr ) {
            delete[] data_unit;
            throw Poco::Exception("Lost connection to peer", openSSLMessage(), SSLErrorCode::LOST_CONNECTION);
        }

        int fd = SSL_get_fd(_sslObject);
        while (!done) {
            FD_ZERO(&readfds);
            FD_ZERO(&writefds);
            FD_SET(fd, &writefds);
            if (write_blocked_on_read) {
                // Read the comments on SSL_ERROR_WANT_READ below
                FD_SET(fd, &readfds);
                has_data = select(fd+1, &readfds, &writefds, nullptr, &to);
            } else {
                // Wait for some room on the network write buffer
                has_data = select(fd+1, nullptr, &writefds, nullptr, &to);
            }
            if (has_data == 0) {
                delete[] data_unit;
                throw Poco::Exception("Write Operation Timeout", SSLErrorCode::WRITE_TIMEOUT);
            } else if (has_data < 0) {
                delete[] data_unit;
                throw Poco::Exception("Could not write the data unit", SSLErrorCode::SSL_WRITE_ERR);
            }

            if (FD_ISSET(fd, &writefds) ||
                (write_blocked_on_read && FD_ISSET(fd, &readfds))) {
                write_blocked_on_read = false;

                int w = SSL_write(_sslObject, data_unit, TOTAL_LENGTH);
                switch (SSL_get_error(_sslObject, w)) {
                    case SSL_ERROR_NONE:
                        done = true;
                        break;
                    case SSL_ERROR_WANT_WRITE:
                        // We've got unflushed data in the SSL buffer as there was no
                        // more room on the network write buffer. As we're using
                        // non-blocking I/O, we just need to wait on select() and try
                        // SSL_write() again.
                        break;
                    case SSL_ERROR_WANT_READ:
                        // This happens when we're trying to rehandshake and we
                        // block on a read during that rehandshake. Another
                        // select() call for waiting the socket to be readable is
                        // needed. As soon as it is, reinitiate the write.
                        write_blocked_on_read = true;
                        break;
                    case SSL_ERROR_ZERO_RETURN:
                        // shutdown and exception
                        SSL_shutdown(_sslObject);
                    default:
                        delete[] data_unit;
                        throw Poco::Exception("Could not write the data unit", SSLErrorCode::SSL_WRITE_ERR);
                }
            }
        }

        delete[] data_unit;
        BIO_flush(_connectionSocket);
    }

    void OpenSSLTLSConnection::init(
        const std::string rootCertificate,
        const std::string clientCertificate,
        const std::string passphrase)
    {
        // SSL_library_init
        // Return Value: Always 1 (nothing to be tested)
        SSL_library_init();

        // SSL_load_error_strings
        // Return Value: Void (nothing to be tested)
        SSL_load_error_strings();

        seed_prng();

        _sslContext = SSL_CTX_new(SSLv23_client_method());

        if (SSL_CTX_load_verify_locations(_sslContext, rootCertificate.c_str(), nullptr) != 1)
            throw Poco::Exception("Error loading CA Certificate file", openSSLMessage(), SSLErrorCode::LOAD_ROOTCA_FILE_ERR);

        if (SSL_CTX_use_certificate_chain_file(_sslContext, clientCertificate.c_str()) != 1)
            throw Poco::Exception("Error loading certificate from file", openSSLMessage(), SSLErrorCode::LOAD_CERT_FILE_ERR);

        if ( !passphrase.empty() ) {
            _passphrase = passphrase;
            SSL_CTX_set_default_passwd_cb(_sslContext, pem_passwd_cb);
        }

        if (SSL_CTX_use_PrivateKey_file(_sslContext, clientCertificate.c_str(), SSL_FILETYPE_PEM) != 1)
            throw Poco::Exception("Error loading private key from file", openSSLMessage(), SSLErrorCode::LOAD_KEY_FILE_ERR);

        SSL_CTX_set_verify(_sslContext, SSL_VERIFY_PEER|SSL_VERIFY_FAIL_IF_NO_PEER_CERT|SSL_VERIFY_CLIENT_ONCE, nullptr);
        SSL_CTX_set_verify_depth(_sslContext, 3);
        SSL_CTX_set_options(_sslContext, SSL_OP_NO_SSLv2|SSL_OP_NO_SSLv3);
    }

    void OpenSSLTLSConnection::nonBlockingIODescriptor()
    {
        if ( _sslObject == nullptr )
            return;

        int fd = SSL_get_fd(_sslObject);
        int flags;
        if ( (flags = fcntl(fd, F_GETFL, 0)) < 0 )
            throw Poco::Exception("Unable to get descriptor status flags", SSLErrorCode::GET_FNCTL_ERR);

        if ( fcntl(fd, F_SETFL, flags | O_NONBLOCK) < 0 )
            throw Poco::Exception("Unable to set SSL underlying I/O descriptor "
                                     "as non-blocking", SSLErrorCode::SET_NONBLOCKING_ERR);
    }

    bool OpenSSLTLSConnection::isCertificateCommonNameOk(const std::string & commonName)
    {
        X509 * cert = nullptr;
        cert = SSL_get_peer_certificate(_sslObject);

        bool ok = false;
        if ( cert != nullptr && !commonName.empty() ) {
            char data[256];
            X509_NAME * subject = nullptr;

            if ((subject = X509_get_subject_name(cert)) != nullptr &&
                X509_NAME_get_text_by_NID(subject, NID_commonName, data, 256) > 0) {
                data[255] = 0;

                if ( strncasecmp(data, commonName.c_str(), 255) == 0 )
                    ok = true;
            }
        }

        if (cert)
            X509_free(cert);

        if (ok)
            return SSL_get_verify_result(_sslObject);

        return X509_V_ERR_APPLICATION_VERIFICATION == X509_V_OK;
    }

    std::string OpenSSLTLSConnection::_passphrase = "";
        ///
        ///
        ///


}
