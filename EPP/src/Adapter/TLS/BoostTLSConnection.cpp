#include "boost/locale.hpp"
#include "boost/array.hpp"
#include "boost/bind.hpp"
#include "boost/asio.hpp"
#include "Poco/Exception.h"
#include "boost/asio/io_service.hpp"
#include "Adapter/TLS/BoostTLSConnection.h"

namespace TLS {


    BoostTLSConnection::BoostTLSConnection(const EPP::EPPConnectionParameters & parameters) :
        _passphrase(parameters.passphrase),
        _ioContext(),
        _sslContext(boost::asio::ssl::context::sslv23_client)
    {
        try {
            boost::asio::ip::tcp::resolver resolver(_ioContext);
            auto endpoint = resolver.resolve(parameters.serverAddress, std::to_string(parameters.serverPort));

            init(parameters.rootCertificate, parameters.clientCertificate);
            helperClass = std::make_shared<NestedHelperConnectionClass>(_ioContext, _sslContext, endpoint);
            _ioContext.run();
        } catch (std::exception & exception) {
            throw Poco::Exception(exception.what());
        }
    }

    BoostTLSConnection::~BoostTLSConnection()
    {}

    void BoostTLSConnection::disconnect()
    {
       helperClass->close();
    }

    std::string BoostTLSConnection::receive()
    {
        return helperClass->read();
    }

    void BoostTLSConnection::send(const std::string & payload)
    {
        helperClass->write(payload);
    }

    std::string BoostTLSConnection::password_callback()
    {
        std::string password = _passphrase;
        return password;
    }

    void BoostTLSConnection::init(const std::string & rootCertificate, const std::string & clientCertificate)
    {
        // TODO set timeouts
        _sslContext.load_verify_file(rootCertificate);
        _sslContext.use_certificate_chain_file(clientCertificate);
        _sslContext.set_password_callback(boost::bind(&BoostTLSConnection::password_callback, this));
        _sslContext.use_private_key_file(clientCertificate, boost::asio::ssl::context_base::file_format::pem);

        _sslContext.set_verify_mode(
            boost::asio::ssl::verify_peer|
            boost::asio::ssl::verify_fail_if_no_peer_cert|
            boost::asio::ssl::verify_client_once
        );
        _sslContext.set_verify_depth(3);
        _sslContext.set_options(boost::asio::ssl::context_base::no_sslv2|boost::asio::ssl::context_base::no_sslv3);
    }


    /**
     * NestedHelperClass
     */
    BoostTLSConnection
        ::NestedHelperConnectionClass::NestedHelperConnectionClass(
            boost::asio::io_context & ioContext,
            boost::asio::ssl::context & sslContext,
            const boost::asio::ip::tcp::resolver::results_type & endpoint)
            : _tlsSocket(ioContext, sslContext)
        {
            connect(endpoint);
        }

    void BoostTLSConnection
        ::NestedHelperConnectionClass::close()
        {
            boost::system::error_code error;
            _tlsSocket.lowest_layer().close(error);
            if (error)
                throw Poco::Exception(error.message(), error.category().name());
        }

    std::string BoostTLSConnection
        ::NestedHelperConnectionClass::read()
        {
            std::stringstream response;
            char buffer[BUFFER_SIZE];
            boost::system::error_code error;

            auto responseSize = boost::asio::read(
                    _tlsSocket,
                    boost::asio::buffer(buffer),
                    boost::asio::transfer_at_least(4),
                    error
                );

            if ( error )
                throw Poco::Exception(error.message(), error.category().name());

            response.write(buffer, responseSize);
            auto responseContent = response.str();

            if ( responseContent.empty() )
                throw Poco::Exception("Invalid total length of the data unit");

            return responseContent.substr(responseContent.find("<"));
        }

    void BoostTLSConnection
        ::NestedHelperConnectionClass::write(const std::string & content)
        {
            boost::asio::write(_tlsSocket, boost::asio::buffer(content, content.length()));
        }

    void BoostTLSConnection
        ::NestedHelperConnectionClass::connect(const boost::asio::ip::tcp::resolver::results_type & endpoint)
        {
            boost::asio::connect(_tlsSocket.lowest_layer(), endpoint);
            _tlsSocket.handshake(boost::asio::ssl::stream_base::client);
        }


}
