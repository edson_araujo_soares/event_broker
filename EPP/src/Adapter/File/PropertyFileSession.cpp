// #include "Foundation/System/Path.h"
#include "Adapter/File/PropertyFileSession.h"
// #include "Foundation/System/EnvironmentManager.h"
// #include "Foundation/System/ApplicationEnvironment.h"

namespace File {


    PropertyFileSession::PropertyFileSession()
        : sessionFile(nullptr)
    {
        sessionFile = new Poco::Util::PropertyFileConfiguration(environmentFilePath());
        if ( sessionFile->has("opened") )
            return;

        sessionFile->setBool("opened", false);
        sessionFile->save(environmentFilePath());
    }

    void PropertyFileSession::open()
    {
        sessionFile->setBool("opened", true);
        sessionFile->save(environmentFilePath());
    }

    void PropertyFileSession::close()
    {
        sessionFile->setBool("opened", false);
        sessionFile->save(environmentFilePath());
    }

    bool PropertyFileSession::isOpened()
    {
        sessionFile->load(environmentFilePath());
        return sessionFile->has("opened") && sessionFile->getBool("opened");
    }

    std::string PropertyFileSession::lifetime()
    {
        return "";
    }

    std::string PropertyFileSession::environmentFilePath()
    {
        std::string environmentPath;
        // auto environmentName = Foundation::System::EnvironmentManager::getEnvironment();

        // if ( Foundation::System::ApplicationEnvironment::DEBUG == environmentName )
        //    environmentPath = Foundation::System::Path::APPLICATION_BASE_DIRECTORY + "/" + SESSION_FILE_NAME;

        // if ( Foundation::System::ApplicationEnvironment::PRODUCTION == environmentName )
        //    environmentPath = Foundation::System::Path::APPLICATION_CONFIGURATION_DIRECTORY + "/" + SESSION_FILE_NAME;

        //if ( Foundation::System::ApplicationEnvironment::HOMOLOGATION == environmentName )
        //    environmentPath = Foundation::System::Path::APPLICATION_CONFIGURATION_DIRECTORY + "/" +SESSION_FILE_NAME;

        return environmentPath;
    }


}
