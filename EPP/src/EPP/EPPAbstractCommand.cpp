#include "EPP/EPPAbstractCommand.h"

namespace EPP {

    EPPAbstractCommand::EPPAbstractCommand()
        : xmlData()
    {}

    EPPAbstractCommand::
        EPPCommandHandler::EPPCommandHandler(EPP::EPPAbstractCommand * command)
         : _command(command),
           templatesDirectory("")
    {}

    std::unique_ptr<EPPAbstractCommand::EPPCommandHandler> EPPAbstractCommand::handler()
    {
        // Dirty work; Injects Command[this], EventAggregator
        return std::make_unique<EPPCommandHandler>(this);
    }

    std::shared_ptr<EPPConnectionInterface> EPPAbstractCommand::connection()
    {
        return _eppConnection;
    }


}
