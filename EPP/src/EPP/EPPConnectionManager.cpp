#include "Poco/Exception.h"
#include "EPP/EPPConnectionManager.h"
#include "Adapter/TLS/BoostTLSConnection.h"
#include "Adapter/TLS/OpenSSLTLSConnection.h"

namespace EPP {


    std::unique_ptr<EPP::EPPConnectionInterface> EPPConnectionManager::connect(
        EPPConnectionAdapter adapter,
        const EPP::EPPConnectionParameters & parameters)
    {
        std::unique_ptr<EPP::EPPConnectionInterface> eppConnection = nullptr;
        try {
            switch (adapter) {
                case RAW_OPENSSL:
                    eppConnection = std::make_unique<TLS::OpenSSLTLSConnection>(parameters);
                    break;

                case TLS_WITH_BOOST:
                    eppConnection = std::make_unique<TLS::BoostTLSConnection>(parameters);
                    break;
            }
        } catch (Poco::Exception &) {
            throw;
        }

        return eppConnection;
    }


}
