#ifndef EPP_EPPConnectionInterface_INCLUDED
#define EPP_EPPConnectionInterface_INCLUDED

#include <string>

namespace EPP {


    class EPPConnectionInterface
    {
    public:
        virtual ~EPPConnectionInterface() = default;
        virtual void disconnect() = 0;
        virtual std::string receive() = 0;
        virtual void send(const std::string & payload) = 0;

    };


}

#endif
