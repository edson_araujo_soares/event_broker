#ifndef EPP_EPPConnectionParameters_INCLUDED
#define EPP_EPPConnectionParameters_INCLUDED

#include <string>

namespace EPP {


    struct EPPConnectionParameters {
        std::string rootCertificate;
        std::string clientCertificate;
        std::string serverAddress;
        std::string passphrase;
        int serverPort;

    };


}

#endif
