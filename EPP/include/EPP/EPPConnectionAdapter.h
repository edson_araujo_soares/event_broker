#ifndef EPP_EPPConnectionAdapter_INCLUDED
#define EPP_EPPConnectionAdapter_INCLUDED

#include <string>

namespace EPP {


    enum EPPConnectionAdapter {

        RAW_OPENSSL     = 1000,
        TLS_WITH_BOOST  = 1001,

    };


}

#endif
