#ifndef EPP_EPPConnectionManager_INCLUDED
#define EPP_EPPConnectionManager_INCLUDED

#include <string>
#include <memory>
#include "EPP/EPPConnectionAdapter.h"
#include "EPP/EPPConnectionInterface.h"
#include "EPP/EPPConnectionParameters.h"

namespace EPP {


    class EPPConnectionManager
    {
    public:
        static std::unique_ptr<EPP::EPPConnectionInterface> connect(EPPConnectionAdapter, const EPPConnectionParameters &);

    };


}

#endif
