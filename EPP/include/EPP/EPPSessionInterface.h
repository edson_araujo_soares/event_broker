#ifndef EPP_EPPSessionInterface_INCLUDED
#define EPP_EPPSessionInterface_INCLUDED

#include <string>

namespace EPP {


    class EPPSessionInterface
    {
    public:
        virtual ~EPPSessionInterface() = default;
        virtual void open() = 0;
        virtual void close() = 0;
        virtual bool isOpened() = 0;
        virtual std::string lifetime() = 0;

    };


}

#endif
