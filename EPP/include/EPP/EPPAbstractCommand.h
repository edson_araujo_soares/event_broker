#ifndef EPP_EPPAbstractCommand_INCLUDED
#define EPP_EPPAbstractCommand_INCLUDED

#include <memory>
#include <string>
#include "EPP/EPPCommandInterface.h"
#include "EPP/EPPConnectionInterface.h"

namespace EPP {


    class EPPAbstractCommand : public EPPCommandInterface
    {
    public:

        /*
         *
         */
        class EPPCommandHandler {
        public:
            explicit EPPCommandHandler(EPPAbstractCommand * command);
            void handle() {
                // Inject Command dependencies (XML, OpenSSLTLSConnection, XMLParser)
                _command->xmlData = templatesDirectory + _command->templateFileName();
                _command->_eppConnection = nullptr;
                _command->execute();

                // Handle EPPResponse with Events
            }

        private:
            EPPAbstractCommand * _command;
            std::string templatesDirectory;

        };

        EPPAbstractCommand();
        ~EPPAbstractCommand() override = default;
        std::unique_ptr<EPPCommandHandler> handler();

    protected:
        std::shared_ptr<EPPConnectionInterface> connection();

    private:
        std::string xmlData;
        std::shared_ptr<EPPConnectionInterface> _eppConnection;

    };


}

#endif
