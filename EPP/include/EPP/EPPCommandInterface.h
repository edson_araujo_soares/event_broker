#ifndef EPP_EPPCommandInterface_INCLUDED
#define EPP_EPPCommandInterface_INCLUDED

#include <string>

namespace EPP {


    class EPPCommandInterface
    {
    public:
        virtual ~EPPCommandInterface() = default;
        virtual void execute() = 0;
        virtual std::string templateFileName() = 0;

    };


}

#endif
