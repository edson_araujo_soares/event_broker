#ifndef EPP_Command_Login_INCLUDED
#define EPP_Command_Login_INCLUDED

#include <string>
#include "EPP/EPPAbstractCommand.h"

namespace EPP {


    class Login : public EPPAbstractCommand
    {
    public:
        void execute() override;
        std::string templateFileName() override;

        void identity(const std::string &);
        void password(const std::string &);
        void textLanguage(const std::string &);
        void protocolVersion(const std::string &);

    private:
        std::string _clientID;
        std::string _textLanguage;
        std::string _clientPassword;
        std::string _protocolVersion;

    };


}

#endif
