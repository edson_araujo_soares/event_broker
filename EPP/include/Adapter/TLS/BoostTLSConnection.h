#ifndef Adapter_TSL_BoostTLSConnection_INCLUDED
#define Adapter_TSL_BoostTLSConnection_INCLUDED

#include <string>
#include <memory>
#include "boost/asio/ip/tcp.hpp"
#include "boost/asio/ssl/stream.hpp"
#include "EPP/EPPConnectionInterface.h"
#include "EPP/EPPConnectionParameters.h"

namespace TLS {


    class BoostTLSConnection : public EPP::EPPConnectionInterface
    {
    public:
        explicit BoostTLSConnection(const EPP::EPPConnectionParameters &);
        ~BoostTLSConnection() override;
        void disconnect() override;
        std::string receive() override;
        void send(const std::string & payload) override;

    private:
        /**
         * [...]
         */
        class NestedHelperConnectionClass
        {
            public:
                NestedHelperConnectionClass() = delete;
                NestedHelperConnectionClass(
                    boost::asio::io_context &,
                    boost::asio::ssl::context &,
                    const boost::asio::ip::tcp::resolver::results_type & endpoint
                );

                void close();
                std::string read();
                void write(const std::string &);

            private:
                boost::asio::ssl::stream<boost::asio::ip::tcp::socket> _tlsSocket;

                void connect(const boost::asio::ip::tcp::resolver::results_type & endpoint);

        };

        static const uint32_t BUFFER_SIZE       = 2049;
        static const int CONNECTION_TIMEOUT     = 5;
        static const int WRITE_AND_READ_TIMEOUT = 60;

        const std::string _passphrase;
        boost::asio::io_context _ioContext;
        boost::asio::ssl::context _sslContext;
        std::shared_ptr<NestedHelperConnectionClass> helperClass;

        std::string password_callback();
        void init(const std::string & rootCertificate, const std::string & clientCertificate);

    };


}

#endif
