#ifndef Adapter_TSL_OpenSSLTLSConnection_INCLUDED
#define Adapter_TSL_OpenSSLTLSConnection_INCLUDED

#include <string>
#include <memory>
#include <openssl/bio.h>
#include <openssl/ssl.h>
#include "EPP/EPPConnectionInterface.h"
#include "EPP/EPPConnectionParameters.h"

namespace TLS {


    class OpenSSLTLSConnection : public EPP::EPPConnectionInterface
    {
    public:
        /// SSL Transport Error Code
        enum SSLErrorCode {
            BIO_DO_CONNECT_ERR      = 1000,
            LOST_CONNECTION         = 1001,
            SSL_NEW_ERR             = 1002,
            SSL_CONNECT_ERR         = 1003,
            SSL_READ_ERR            = 1004,
            SSL_WRITE_ERR           = 1005,
            READ_TIMEOUT            = 1006,
            WRITE_TIMEOUT           = 1007,
            MISSING_TOTAL_LENGTH    = 1008,
            INVALID_TOTAL_LENGTH    = 1009,
            PAYLOAD_INCOMPLETE      = 1010,
            LOAD_CERT_FILE_ERR      = 1011,
            LOAD_KEY_FILE_ERR       = 1012,
            GET_FNCTL_ERR           = 1013,
            SET_NONBLOCKING_ERR     = 1014,
            LOAD_ROOTCA_FILE_ERR    = 1015,
            PEER_CERTIFICATE_CN_ERR = 1016
        };

        explicit OpenSSLTLSConnection(const EPP::EPPConnectionParameters &);
        ~OpenSSLTLSConnection() override;
        void disconnect() override;
        std::string receive() override;
        void send(const std::string & payload) override;

    private:
        static std::string _passphrase;
        static const int CONNECTION_TIMEOUT     = 5;
        static const int WRITE_AND_READ_TIMEOUT = 60;

        SSL * _sslObject;
        SSL_CTX * _sslContext;
        BIO * _connectionSocket;
        bool certificateCommonNameCheckingEnabled;

        /// Seed OpenSSL pseudo random number generator
        void seed_prng();

        /// Callback method used by OpenSSL to collect passphrases.
        /**
           @param buf buffer that the passphrase should be copied into
           @param size size of buf in bytes, including the NULL terminating character
           @param rwflag indicates whether the callback is used for reading/decryption
           (rwflag=0) or writing/decryption (rwflag=1)
           @param userdata application specific data

           @return the actual length of the password
        */
        static int pem_passwd_cb(char *buf, int size, int rwflag, void *userdata);

        /// Return the OpenSSL error message
        /**
           @return OpenSSL error message
        */
        std::string openSSLMessage() const;

        void nonBlockingIODescriptor();
        bool isCertificateCommonNameOk(const std::string & commonName);
        void init(const std::string rootCertificate, const std::string clientCertificate, const std::string passphrase);

    };


}

#endif
