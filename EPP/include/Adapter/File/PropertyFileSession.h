#ifndef Adapter_File_PropertyFileSession_INCLUDED
#define Adapter_File_PropertyFileSession_INCLUDED

#include <string>
#include "EPP/EPPSessionInterface.h"
#include "Poco/Util/PropertyFileConfiguration.h"

namespace File {


    class PropertyFileSession : public EPP::EPPSessionInterface
    {
    public:
        PropertyFileSession();
        ~PropertyFileSession() override = default;
        void open() override;
        void close() override;
        bool isOpened() override;
        std::string lifetime() override;

    private:
        Poco::Util::PropertyFileConfiguration * sessionFile;

        std::string const SESSION_FILE_NAME = "session.properties";

        std::string environmentFilePath();

    };


}

#endif
