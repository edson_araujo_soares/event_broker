#ifndef ACL_Command_Connect_INCLUDED
#define ACL_Command_Connect_INCLUDED

#include <string>

#include "Foundation/EPP/EPPCommandInterface.h"

namespace ACL {
namespace Command {


    class Connect : public Foundation::EPPCommandInterface
    {
    public:
        Connect() = delete;
        Connect(
            int port,
            const std::string & server,
            const std::string & passphrase,
            const std::string & rootCertificate,
            const std::string & clientCertificate
        );
        ~Connect() override = default;
        void execute() override;

    private:
        int _port;
        std::string _server;
        std::string _passphrase;
        std::string _rootCertificate;
        std::string _clientCertificate;

    };


} }

#endif
