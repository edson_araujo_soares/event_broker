#ifndef ACL_Command_Login_INCLUDED
#define ACL_Command_Login_INCLUDED

#include <string>
#include <memory>

#include "Foundation/EPP/EPPCommandInterface.h"

namespace ACL {
namespace Command {


    class Login  : public Foundation::EPPCommandInterface
    {
    public:
        Login() = delete;
        Login(const std::string & identity, const std::string & password, const std::string & language);
        ~Login() override = default;

        void execute() override;

    private:
        std::string _identity;
        std::string _password;
        std::string _language;

    };


} }

#endif
