#ifndef ACL_Command_Logout_INCLUDED
#define ACL_Command_Logout_INCLUDED

#include "Foundation/EPP/EPPCommandInterface.h"

namespace ACL {
namespace Command {


    class Logout : public Foundation::EPPCommandInterface
    {
    public:
        Logout() = default;
        ~Logout() override = default;

        void execute() override;

    };


} }

#endif
