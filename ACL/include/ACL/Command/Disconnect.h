#ifndef ACL_Command_Disconnect_INCLUDED
#define ACL_Command_Disconnect_INCLUDED

#include "Foundation/EPP/EPPCommandInterface.h"

namespace ACL {
namespace Command {


    class Disconnect : public Foundation::EPPCommandInterface
    {
    public:
        Disconnect() = default;
        ~Disconnect() override = default;

        void execute() override;

    };


} }

#endif
