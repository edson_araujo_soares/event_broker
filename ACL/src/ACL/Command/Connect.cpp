#include "Poco/Exception.h"
// #include "ACL/EPP/Session.h"
#include "ACL/Command/Connect.h"

namespace ACL {
namespace Command {


    Connect::Connect(
        int port,
        const std::string & server,
        const std::string & passphrase,
        const std::string & rootCertificate,
        const std::string & clientCertificate
    ) : _port(port),
        _server(server),
        _passphrase(passphrase),
        _rootCertificate(rootCertificate),
        _clientCertificate(clientCertificate)
    {}

    void Connect::execute()
    {
        try {
            // auto &instance = EPP::Session::instance();

            // instance.init(_server, _port);
            // instance.connect(_clientCertificate, _rootCertificate, _passphrase);
        } catch (Poco::Exception & exception) {
            throw;
        }
    }


} }
