#include "Gateway/Resource/Entrypoint.h"
#include "Gateway/Handling/EntrypointJSONBuilder.h"

namespace Gateway {


    void Entrypoint::handleRequest(Poco::Net::HTTPServerRequest & request, Poco::Net::HTTPServerResponse & response)
    {
        response.setStatus(Poco::Net::HTTPResponse::HTTP_OK);
        response.setContentType("application/vnd.api+json");

        std::ostream & outputStream = response.send();

        Handling::EntrypointJSONBuilder jsonBuilder = Handling::EntrypointJSONBuilder(request.getHost());
        outputStream << jsonBuilder.getJson("prototype");
        outputStream.flush();
    }


}
