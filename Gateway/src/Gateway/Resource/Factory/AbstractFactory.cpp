#include "Gateway/Resource/Factory/AbstractFactory.h"
#include "Gateway/Resource/Factory/EntrypointFactory.h"
#include "IdentityAccess/Adapter/Http/Resource/Factory/SessionFactory.h"
#include "IdentityAccess/Adapter/Http/Resource/Factory/AuthenticationFactory.h"

namespace Gateway {
namespace Resource {


	std::unique_ptr<Foundation::ResourceFactoryInterface> AbstractFactory::createResourceFactory(const std::string & resourceName)
	{
		std::unique_ptr<Foundation::ResourceFactoryInterface> resourceFactory = nullptr;

        if ( resourceName == "Entrypoint::Resource::Entrypoint" )
            resourceFactory = std::make_unique<EntrypointFactory>();

        if ( resourceName == "IdentityAccess::Resource::EPP" )
            resourceFactory = std::make_unique<Http::Resource::SessionFactory>();

        if ( resourceName == "IdentityAccess::Resource::Authentication" )
            resourceFactory = std::make_unique<Http::Resource::AuthenticationFactory>();

		return resourceFactory;
	}


} }
