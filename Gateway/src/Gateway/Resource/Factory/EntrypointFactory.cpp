#include "Gateway/Resource/Entrypoint.h"
#include "Gateway/Resource/Factory/EntrypointFactory.h"

namespace Gateway {
namespace Resource {


    Poco::Net::HTTPRequestHandler * EntrypointFactory::createResource()
    {
        return new Entrypoint();
    }


} }
