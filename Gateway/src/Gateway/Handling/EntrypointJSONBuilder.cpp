#include "Poco/JSON/Object.h"
#include "Gateway/Handling/EntrypointJSONBuilder.h"

namespace Gateway {
namespace Handling {


    EntrypointJSONBuilder::EntrypointJSONBuilder(const std::string & baseUrl)
        : _baseUrl(baseUrl)
    {}

    std::string EntrypointJSONBuilder::getUrl(const std::string & path) const
    {
        return "http://" + _baseUrl + path;
    }

    std::string EntrypointJSONBuilder::getJson(const std::string & version) const
    {
        // { ... }
        Poco::JSON::Object::Ptr root = new Poco::JSON::Object();

        // "links" : { ... }
	    Poco::JSON::Object::Ptr links = new Poco::JSON::Object();

        links->set("self",                  getUrl("/"));
        links->set("session_invalidation",  getUrl("/session"));
        links->set("client_authentication", getUrl("/authentication"));

        root->set("links", links);

        // "meta" : { ... }
	    Poco::JSON::Object::Ptr meta = new Poco::JSON::Object();

	    meta->set("version",     version);
	    meta->set("copyright",   "Copyright 2019, Impactante LTDA.");

        // "lang" : [ .. ]
	    Poco::JSON::Array::Ptr langs = new Poco::JSON::Array();

        langs->set(0, "en-US");
        langs->set(1, "pt-BR");

        meta->set("lang", langs);
	    root->set("meta", meta);

        std::stringstream stream;
        root->stringify(stream);
        return stream.str();
    }


} }
