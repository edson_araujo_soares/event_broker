#include "Poco/ClassLibrary.h"
#include "Gateway/RouterEntrypoint.h"
#include "Gateway/Resource/Factory/AbstractFactory.h"

namespace Gateway {


    void RouterEntrypoint::loadEndpoints()
    {
        addEndpoint("/",               "Entrypoint::Resource::Entrypoint");
        addEndpoint("/session",        "IdentityAccess::Resource::EPP");
        addEndpoint("/authentication", "IdentityAccess::Resource::Authentication");
    }

    std::unique_ptr<Foundation::RouterAbstractFactoryInterface> RouterEntrypoint::getAbstractFactory()
    {
        return std::make_unique<Resource::AbstractFactory>();
    }


}

// add support to Poco ApacheConnector
POCO_BEGIN_MANIFEST(Poco::Net::HTTPRequestHandlerFactory)
    POCO_EXPORT_CLASS(Gateway::RouterEntrypoint)
POCO_END_MANIFEST
