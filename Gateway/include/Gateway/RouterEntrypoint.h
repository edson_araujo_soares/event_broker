#ifndef Gateway_RouterEntrypoint_INCLUDED
#define Gateway_RouterEntrypoint_INCLUDED

#include "Foundation/Http/Router.h"
#include "Foundation/Http/RouterAbstractFactoryInterface.h"

namespace Gateway {


    class RouterEntrypoint : public Foundation::Router
    {
    public:
        void loadEndpoints() override;
        std::unique_ptr<Foundation::RouterAbstractFactoryInterface> getAbstractFactory() override;

    };


}

#endif
