#ifndef Gateway_Handling_EntrypointJSONBuilder_INCLUDED
#define Gateway_Handling_EntrypointJSONBuilder_INCLUDED

#include <string>

namespace Gateway {
namespace Handling {


    class EntrypointJSONBuilder
    {
    public:
        explicit EntrypointJSONBuilder(const std::string & baseUrl);
        std::string getJson(const std::string & version) const;

    private:
        std::string _baseUrl;

        std::string getUrl(const std::string &) const;

    };


} }

#endif
