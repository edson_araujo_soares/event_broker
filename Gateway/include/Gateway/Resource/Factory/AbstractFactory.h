#ifndef Gateway_Resource_AbstractFactory_INCLUDED
#define Gateway_Resource_AbstractFactory_INCLUDED

#include <string>

#include "Foundation/Http/RouterAbstractFactoryInterface.h"

namespace Gateway {
namespace Resource {


    class AbstractFactory : public Foundation::RouterAbstractFactoryInterface
    {
    public:
    	~AbstractFactory() override = default;
        std::unique_ptr<Foundation::ResourceFactoryInterface> createResourceFactory(const std::string & resourceName) override;

    };


} }

#endif
