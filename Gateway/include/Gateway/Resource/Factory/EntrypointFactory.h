#ifndef Gateway_Resource_Factory_EntrypointFactory_INCLUDED
#define Gateway_Resource_Factory_EntrypointFactory_INCLUDED

#include "Poco/Net/HTTPRequestHandler.h"
#include "Foundation/Http/ResourceFactoryInterface.h"

namespace Gateway {
namespace Resource {


    class EntrypointFactory : public Foundation::ResourceFactoryInterface
    {
    public:
        ~EntrypointFactory() override = default;
        Poco::Net::HTTPRequestHandler * createResource() override;

    };


} }

#endif
