#ifndef Gateway_Resource_Entrypoint_INCLUDED
#define Gateway_Resource_Entrypoint_INCLUDED

#include "Poco/Net/HTTPServerRequest.h"
#include "Poco/Net/HTTPServerResponse.h"
#include "Poco/Net/HTTPRequestHandler.h"

namespace Gateway {


    class Entrypoint : public Poco::Net::HTTPRequestHandler
    {
    public:
        ~Entrypoint() override = default;
        void handleRequest(Poco::Net::HTTPServerRequest &, Poco::Net::HTTPServerResponse &) override;

    };


}

#endif
