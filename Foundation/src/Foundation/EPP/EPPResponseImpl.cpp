#include "Poco/DOM/NodeList.h"
#include "Poco/DOM/Document.h"
#include "Poco/DOM/DOMParser.h"
#include "Foundation/EPP/EPPResponseImpl.h"

namespace Foundation {


    EPPResponseImpl::EPPResponseImpl(std::string rawResponse)
        : _code(-1),
          _reason(),
          _message(),
          _rawResponse(std::move(rawResponse)),
          metadataPool {
              { "msg",         ""   },
              { "lang",        "en" },
              { "reason",      ""   },
              { "result_code", "0"  }
          }
    {
        interpret();

        _message = metadata("msg");
        _reason  = metadata("reason");
        _code    = std::stoi(metadata("result_code"));
    }

    void EPPResponseImpl::interpret()
    {
        if ( _rawResponse.empty() )
            return;

        Poco::XML::DOMParser parser;
        Poco::XML::Document * document  = parser.parseString(_rawResponse);

        Poco::XML::NodeList * message = document->getElementsByTagName("msg");
        if ( message != nullptr && message->length() > 0 )
            metadata("msg",  message->item(0)->innerText());

        Poco::XML::Node * sessionID = document->getNodeByPath("/epp/response/trID/clTRID");
        if ( sessionID != nullptr )
            metadata("clTRID", sessionID->innerText());

        Poco::XML::NodeList * reason = document->getElementsByTagName("reason");
        if ( reason != nullptr && reason->length() > 0 )
            metadata("reason", reason->item(0)->innerText());

        Poco::XML::Node * languageCode = document->getNodeByPath("/epp/response/result/msg[@lang]");
        if ( languageCode != nullptr && !languageCode->getNodeValue().empty() )
            metadata("lang", languageCode->getNodeValue());

        Poco::XML::Node * resultCode = document->getNodeByPath("/epp/response/result[@code]");
        if ( resultCode != nullptr && !resultCode->getNodeValue().empty() )
            metadata("result_code", resultCode->getNodeValue());
    }

    EPPResponseStatus EPPResponseImpl::statusCode() const
    {
        return (EPPResponseStatus) _code;
    }

    std::string EPPResponseImpl::reason()
    {
        return _reason;
    }

    std::string EPPResponseImpl::message()
    {
        return _message;
    }

    std::string EPPResponseImpl::rawResponse()
    {
        return _rawResponse;
    }

    std::string EPPResponseImpl::metadata(const std::string & key)
    {
        std::string metadata;
        try {
             metadata = metadataPool.at(key);
        } catch (std::out_of_range & exception) {
            throw Poco::InvalidArgumentException(exception.what());
        }

        return metadata;
    }

    void EPPResponseImpl::metadata(const std::string & key, const std::string & value)
    {
        metadataPool[key] = value;
    }


}
