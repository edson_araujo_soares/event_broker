#include <typeinfo>
#include <iostream>
#include <cxxabi.h>
#include <algorithm>
#include "Foundation/Event/ThreadSafeEventAggregator.h"

namespace Event {


    ThreadSafeEventAggregator::ThreadSafeEventAggregator()
        : messageQueue(boost::interprocess::open_or_create, "myQueue", 100, 100)
    {}

    ThreadSafeEventAggregator::~ThreadSafeEventAggregator()
    {
        boost::interprocess::message_queue::remove("myQueue");
    }

    void ThreadSafeEventAggregator::publish(std::shared_ptr<Foundation::EventInterface> event)
    {
        const std::type_info &typeInfo = typeid(*event);
        std::string eventClassName     = abi::__cxa_demangle(typeInfo.name(), nullptr, nullptr, nullptr);
        eventClassName                 = eventClassName.substr(eventClassName.find_last_of(':')+1);

        if ( !hasSubscribers(eventClassName) )
            return;

        auto subscribers = subscriptions[eventClassName];
        for ( auto const &item : subscribers )
            item->handle(event);
    }

    void ThreadSafeEventAggregator::subscribe(Foundation::SubscriberInterface & subscriber, const std::string & eventName)
    {
        if ( !hasSubscribers(eventName) )
            subscriptions[eventName] = Subscribers();

        subscriptions[eventName].emplace_back(&subscriber);
    }

    void ThreadSafeEventAggregator::unsubscribe(Foundation::SubscriberInterface & subscriber, const std::string & eventName)
    {
        if ( !hasSubscribers(eventName) )
            return;

        auto position = std::find(
            subscriptions[eventName].begin(),
            subscriptions[eventName].end(),
            &subscriber
        );
        subscriptions[eventName].erase(position);
    }

    bool ThreadSafeEventAggregator::hasSubscribers(const std::string & eventName)
    {
        return subscriptions.find(eventName) != subscriptions.end();
    }


}
