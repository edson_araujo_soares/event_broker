#include "Foundation/Event/StandardEventAggregator.h"
#include "Foundation/Event/EventAggregatorFactory.h"

namespace Foundation {


    std::shared_ptr<EventAggregatorInterface> EventAggregatorFactory::create(const std::string & type)
    {
        std::shared_ptr<EventAggregatorInterface> eventAggregator = nullptr;

        if ( type.empty() || eventAggregator == nullptr )
            eventAggregator = std::make_shared<Event::StandardEventAggregator>();

        return eventAggregator;
    }


}
