#ifndef Foundation_Message_MessageInterface_INCLUDED
#define Foundation_Message_MessageInterface_INCLUDED

#include "Poco/Any.h"

namespace Foundation {


    class MessageInterface
    {
    public:
        virtual ~MessageInterface() = default;
        virtual Poco::Any data() = 0;
        virtual std::string occurredOn() = 0;

    };


}

#endif
