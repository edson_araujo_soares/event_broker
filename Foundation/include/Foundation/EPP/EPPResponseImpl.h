#ifndef Foundation_EPPResponseImpl_INCLUDED
#define Foundation_EPPResponseImpl_INCLUDED

#include <map>

#include "Foundation/EPP/EPPResponseInterface.h"

namespace Foundation {


    class EPPResponseImpl : public EPPResponseInterface
    {
    public:
        explicit EPPResponseImpl(std::string rawResponse);

        std::string reason() override;
        std::string message() override;
        std::string rawResponse() override;
        EPPResponseStatus statusCode() const override;
        std::string metadata(const std::string &) override;
        void metadata(const std::string & key, const std::string & value);

    private:
        int _code;
        std::string _reason;
        std::string _message;
        std::string _rawResponse;
        std::map<std::string, std::string> metadataPool;

        void interpret();

    };


}

#endif
