#ifndef Foundation_EPP_EPPCommandInterface_INCLUDED
#define Foundation_EPP_EPPCommandInterface_INCLUDED

#include <string>

namespace Foundation {


    /**
     * An EPP client interacts with an EPP server by sending a command to the server and receiving a response from the server.
     * This interface represents an EPP Command.
     *
     * EPP provides commands to manage sessions, retrieve object information, and perform transformation operations on
     * objects. All EPP commands are atomic and designed so that they can be made idempotent, either succeeding completely
     * or failing completely and producing predictable results in case of repeated executions.
     */
    class EPPCommandInterface
    {
    public:
        virtual ~EPPCommandInterface() = default;
        virtual void execute() = 0;

    };


}

#endif
