#ifndef Foundation_EPP_EPPResponseStatus_INCLUDED
#define Foundation_EPP_EPPResponseStatus_INCLUDED

#include <string>

namespace Foundation {


    /**
     * There are two values for the first digit of the reply code:
     *
     * 1yzz    Positive completion reply.  The command was accepted and processed by the system without error.
     *
     * 2yzz    Negative completion reply.  The command was not accepted, and the requested action did not occur.
     *
     * The second digit groups responses into one of six specific categories:
     *
     *     x0zz    Protocol Syntax
     *     x1zz    Implementation-specific Rules
     *     x2zz    Security
     *     x3zz    Data Management
     *     x4zz    Server System
     *     x5zz    Connection Management
     *
     * The third and fourth digits provide response detail within the categories defined by the first and second
     * digits. The complete list of valid result codes is enumerated below and in the normative schema.
     *
     * <a href="https://tools.ietf.org/html/rfc5730#section-3">EPP Specification</a>
     */
    enum EPPResponseStatus {

        COMMAND_COMPLETED_SUCCESSFULLY                   = 1000,
        COMMAND_COMPLETED_SUCCESSFULLY_ACTION_PENDING    = 1001,
        COMMAND_COMPLETED_SUCCESSFULLY_NO_MESSAGES       = 1300,
        COMMAND_COMPLETED_SUCCESSFULLY_ACK_TO_DEQUEUE    = 1301,
        COMMAND_COMPLETED_SUCCESSFULLY_ENDING_SESSION    = 1500,

        UNKNOWN_COMMAND                                  = 2000,
        COMMAND_SYNTAX_ERROR                             = 2001,
        COMMAND_USE_ERROR                                = 2002,
        REQUIRED_PARAMETER_MISSING                       = 2003,
        PARAMETER_VALUE_RANGE_ERROR                      = 2004,
        PARAMETER_VALUE_SYNTAX_ERROR                     = 2005,

        UNIMPLEMENTED_PROTOCOL_VERSION                   = 2005,
        UNIMPLEMENTED_COMMAND                            = 2101,
        UNIMPLEMENTED_OPTION                             = 2102,
        UNIMPLEMENTED_EXTENSION                          = 2103,
        BILLING_FAILURE                                  = 2104,
        OBJECT_NOT_ELIGIBLE_FOR_RENEWAL                  = 2105,
        OBJECT_NOT_ELIGIBLE_FOR_TRANSFER                 = 2106,

        AUTHENTICATION_ERROR                             = 2200,
        AUTHORIZATION_ERROR                              = 2201,
        INVALID_AUTHORIZATION_INFORMATION                = 2202,

        OBJECT_PENDING_TRANSFER                          = 2300,
        OBJECT_NOT_PENDING_TRANSFER                      = 2301,
        OBJECT_EXISTS                                    = 2302,
        OBJECT_DOES_NOT_EXISTS                           = 2303,
        OBJECT_STATUS_PROHIBITS_OPERATION                = 2304,
        OBJECT_ASSOCIATION_PROHIBITS_OPERATION           = 2305,
        PARAMETER_VALUE_POLICY_ERROR                     = 2306,
        UNIMPLEMENTED_OBJECT_SERVICE                     = 2307,
        DATA_MANAGEMENT_POLICY_VIOLATION                 = 2308,

        COMMAND_FAILED                                   = 2400,

        COMMAND_FAILED_SERVER_CLOSING_CONNECTION         = 2500,
        AUTHENTICATION_ERROR_SERVER_CLOSING_CONNECTION   = 2501,
        SESSION_LIMIT_EXCEEDED_SERVER_CLOSING_CONNECTION = 2502,

    };


}

#endif
