#ifndef Foundation_EPP_EPPResponseInterface_INCLUDED
#define Foundation_EPP_EPPResponseInterface_INCLUDED

#include <string>
#include "Foundation/EPP/EPPResponseStatus.h"

namespace Foundation {


    class EPPResponseInterface
        /// For learning more about EPP Result Codes see this @see https://tools.ietf.org/html/rfc5730#section-3.
    {
    public:
        virtual ~EPPResponseInterface() = default;

        virtual std::string reason() = 0;
        virtual std::string message() = 0;
        virtual std::string rawResponse() = 0;
        virtual EPPResponseStatus statusCode() const = 0;
        virtual std::string metadata(const std::string &) = 0;
            /// It allows a client gets some metadata value by using its key.
            /// For instance, common metadata often used: language code and token

    };


}

#endif
