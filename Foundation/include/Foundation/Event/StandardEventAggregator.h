#ifndef Foundation_Event_StandardEventAggregator_INCLUDED
#define Foundation_Event_StandardEventAggregator_INCLUDED

#include <map>
#include <vector>
#include <memory>
#include "Foundation/Event/EventInterface.h"
#include "Foundation/Event/SubscriberInterface.h"
#include "Foundation/Event/EventAggregatorInterface.h"

namespace Event {


    class StandardEventAggregator : public Foundation::EventAggregatorInterface
    {
    public:
        ~StandardEventAggregator() override = default;
        void publish(std::shared_ptr<Foundation::EventInterface>) final;
        bool hasSubscribers(const std::string & eventName) final;
        void subscribe(Foundation::SubscriberInterface &, const std::string & eventName) final;
        void unsubscribe(Foundation::SubscriberInterface &, const std::string & eventName) final;

    private:
        typedef std::vector<Foundation::SubscriberInterface *> Subscribers;
        std::map<std::string, Subscribers> subscriptions;

    };


}

#endif
