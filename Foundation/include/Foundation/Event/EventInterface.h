#ifndef Foundation_Event_EventInterface_INCLUDED
#define Foundation_Event_EventInterface_INCLUDED

#include "Poco/Any.h"

namespace Foundation {


    class EventInterface
    {
    public:
        virtual ~EventInterface() = default;
        virtual Poco::Any getData() = 0;
        virtual std::string occurredOn() = 0;

    };


}

#endif
