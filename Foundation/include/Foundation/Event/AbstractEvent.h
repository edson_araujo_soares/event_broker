#ifndef Foundation_Event_AbstractEvent_INCLUDED
#define Foundation_Event_AbstractEvent_INCLUDED

#include <string>
#include <typeinfo>
#include <cxxabi.h>
#include "Poco/DateTime.h"
#include "Poco/DateTimeFormat.h"
#include "Poco/DateTimeFormatter.h"
#include "Foundation/Event/AbstractEvent.h"
#include "Foundation/Event/EventInterface.h"

namespace Foundation {


    template <typename T>
    class ClassNameHandler
    {
    public:
        static const std::string className()
        {
            const std::type_info & typeInfo = typeid(T);
            std::string className           = abi::__cxa_demangle(typeInfo.name(), nullptr, nullptr, nullptr);

            return className.substr(className.find_last_of(':')+1);
        }

    };

    template <typename T = EventInterface>
    class AbstractEvent :
        public EventInterface,
        public ClassNameHandler<T>
    {
    public:
        ~AbstractEvent() override = default;
        std::string occurredOn() override
        {
            return Poco::DateTimeFormatter::format(Poco::DateTime(), Poco::DateTimeFormat::SORTABLE_FORMAT);
        }

    protected:
        AbstractEvent() = default;

    };


}

#endif
