#ifndef Foundation_Event_EventAggregatorFactory_INCLUDED
#define Foundation_Event_EventAggregatorFactory_INCLUDED

#include <map>
#include <vector>
#include <memory>
#include "Foundation/Event/EventAggregatorInterface.h"

namespace Foundation {


    class EventAggregatorFactory
    {
    public:
        static std::shared_ptr<EventAggregatorInterface> create(const std::string & type = "");

    };


}

#endif
