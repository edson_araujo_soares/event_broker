#ifndef Foundation_Event_SubscriberInterface_INCLUDED
#define Foundation_Event_SubscriberInterface_INCLUDED

#include <memory>
#include "Foundation/Event/EventInterface.h"

namespace Foundation {


    class SubscriberInterface
    {
    public:
        virtual ~SubscriberInterface() = default;
        virtual void handle(std::shared_ptr<EventInterface>) = 0;

    };


}

#endif
