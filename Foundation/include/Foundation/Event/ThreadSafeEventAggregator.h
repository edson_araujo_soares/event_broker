#ifndef Foundation_Event_ThreadSafeEventAggregator_INCLUDED
#define Foundation_Event_ThreadSafeEventAggregator_INCLUDED

#include <map>
#include <vector>
#include <memory>

#include "boost/interprocess/ipc/message_queue.hpp"
#include "boost/interprocess/managed_mapped_file.hpp"

#include "Foundation/Event/EventInterface.h"
#include "Foundation/Event/SubscriberInterface.h"
#include "Foundation/Event/EventAggregatorInterface.h"

namespace Event {


    class ThreadSafeEventAggregator : public Foundation::EventAggregatorInterface
    {
    public:
        ThreadSafeEventAggregator();
        ~ThreadSafeEventAggregator() override;
        void publish(std::shared_ptr<Foundation::EventInterface>) final;
        bool hasSubscribers(const std::string & eventName) final;
        void subscribe(Foundation::SubscriberInterface &, const std::string & eventName) final;
        void unsubscribe(Foundation::SubscriberInterface &, const std::string & eventName) final;

    private:
        typedef std::vector<Foundation::SubscriberInterface *> Subscribers;
        std::map<std::string, Subscribers> subscriptions;

        boost::interprocess::message_queue messageQueue;

    };


}

#endif
