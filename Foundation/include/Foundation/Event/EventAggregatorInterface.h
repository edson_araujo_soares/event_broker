#ifndef Foundation_Event_EventAggregatorInterface_INCLUDED
#define Foundation_Event_EventAggregatorInterface_INCLUDED

#include <memory>
#include "Foundation/Event/EventInterface.h"
#include "Foundation/Event/SubscriberInterface.h"

namespace Foundation {


    class EventAggregatorInterface
    {
    public:
        virtual ~EventAggregatorInterface() = default;
        virtual void publish(std::shared_ptr<EventInterface>) = 0;
        virtual bool hasSubscribers(const std::string & eventName) = 0;
        virtual void subscribe(SubscriberInterface &, const std::string & eventName) = 0;
        virtual void unsubscribe(SubscriberInterface &, const std::string & eventName) = 0;

    };


}

#endif
