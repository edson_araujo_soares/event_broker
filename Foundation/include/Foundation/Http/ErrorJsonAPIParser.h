#ifndef Foundation_Http_ErrorJSONParser_INCLUDED
#define Foundation_Http_ErrorJSONParser_INCLUDED

#include <string>

#include "Foundation/Http/ErrorParserInterface.h"

namespace Foundation {


    class ErrorJsonAPIParser : public Foundation::ErrorParserInterface
    {
    public:
        std::string toJson(
            const std::string & host,
            const std::string & code,
            const std::string & path,
            const std::string & type,
            const std::string & description
        ) override;

    };


}

#endif
