#ifndef Foundation_Http_DefaultNotFoundErrorHandler_INCLUDED
#define Foundation_Http_DefaultNotFoundErrorHandler_INCLUDED

#include "Poco/Net/HTTPServerRequest.h"
#include "Poco/Net/HTTPRequestHandler.h"
#include "Poco/Net/HTTPServerResponse.h"

namespace Foundation {


    class DefaultNotFoundErrorHandler : public Poco::Net::HTTPRequestHandler
    {
    public:
        void handleRequest(Poco::Net::HTTPServerRequest &, Poco::Net::HTTPServerResponse &) override;

    };


}

#endif
