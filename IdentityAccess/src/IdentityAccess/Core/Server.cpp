#include "Poco/Exception.h"
#include "IdentityAccess/Core/Server.h"

namespace Core {


    Server::Server(
        int port,
        const std::string & url,
        const std::string & passphrase,
        const std::string & rootCertificate,
        const std::string & clientCertificate
    ) : _port(port),
        _url(url),
        _passphrase(passphrase),
        _rootCertificate(rootCertificate),
        _clientCertificate(clientCertificate)
    {
        try {
            poco_assert_msg(!url.empty(),               "URL cannot be empty.");
            poco_assert_msg(!passphrase.empty(),        "Passphrase cannot be empty.");
            poco_assert_msg(!rootCertificate.empty(),   "Root Certificate cannot be empty.");
            poco_assert_msg(!clientCertificate.empty(), "Client Certificate cannot be empty.");
        } catch (Poco::Exception & exception) {
            throw Poco::InvalidArgumentException(exception.message(), exception.code());
        }
    }

    int Server::port()
    {
        return _port;
    }

    std::string Server::url()
    {
        return _url;
    }

    std::string Server::passphrase()
    {
        return _passphrase;
    }

    std::string Server::rootCertificate()
    {
        return _rootCertificate;
    }

    std::string Server::clientCertificate()
    {
        return _clientCertificate;
    }


}
