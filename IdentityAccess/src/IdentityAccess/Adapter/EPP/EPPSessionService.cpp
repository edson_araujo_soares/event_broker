#include "Poco/Exception.h"
// #include "IdentityAccess/Core/SessionInvalidated.h"
#include "IdentityAccess/Adapter/EPP/EPPSessionService.h"

namespace EPP {


    EPPSessionService::EPPSessionService(std::shared_ptr<Foundation::EventAggregatorInterface> aggregator)
        : _eventAggregator(std::move(aggregator))
    {}

    void EPPSessionService::invalidate()
    {
        try {
            // auto logoutCommand = std::make_unique<ACL::Command::Logout>();
            // logoutCommand->execute();

            // auto disconnectCommand = std::make_unique<ACL::Command::Disconnect>();
            // disconnectCommand->execute();

            // _eventAggregator->publish(std::make_shared<Core::SessionInvalidated>());

        } catch (Poco::InvalidAccessException & exception) {
            throw Poco::InvalidAccessException("Unauthorized", "There is no opened session.", 401);
        } catch (Poco::Exception & exception) {
            throw;
        }
    }


}
