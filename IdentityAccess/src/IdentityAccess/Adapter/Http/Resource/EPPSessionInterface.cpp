#include <string>

#include "Poco/URI.h"
#include "Poco/Exception.h"
#include "Poco/StreamCopier.h"
#include "Poco/Net/HTTPBasicCredentials.h"
#include "IdentityAccess/Adapter/Http/Resource/Session.h"

namespace Http {
namespace Resource {


    void Session::handle_delete(Poco::Net::HTTPServerRequest &, Poco::Net::HTTPServerResponse & response)
    {
        sessionService->invalidate();
        response.setStatus(Poco::Net::HTTPResponse::HTTP_OK);
    }

    void Session::handle_options(Poco::Net::HTTPServerRequest &, Poco::Net::HTTPServerResponse & response)
    {
        response.set("Allow", "DELETE, OPTIONS");
        response.setStatus(Poco::Net::HTTPResponse::HTTP_OK);
    }

    void Session::setSessionService(std::shared_ptr<Core::SessionServiceInterface> service)
    {
    	sessionService = std::move(service);
    }


} }
