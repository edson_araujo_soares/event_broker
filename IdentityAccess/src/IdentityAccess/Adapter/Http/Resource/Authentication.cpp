#include <string>

#include "Poco/URI.h"
#include "Poco/Exception.h"
#include "Poco/StreamCopier.h"
#include "Poco/Net/HTTPBasicCredentials.h"
#include "IdentityAccess/Adapter/Http/Resource/Authentication.h"

namespace Http {
namespace Resource {


    void Authentication::handle_post(Poco::Net::HTTPServerRequest & request, Poco::Net::HTTPServerResponse & response)
    {
        Poco::Net::HTTPBasicCredentials credentials(request);
        authenticationService->authenticate(credentials.getUsername(), credentials.getPassword(), request.get("Accept-Language"));

        response.setStatus(Poco::Net::HTTPResponse::HTTP_OK);
        response.set("Content-Language", request.get("Accept-Language"));
    }

    void Authentication::handle_options(Poco::Net::HTTPServerRequest &, Poco::Net::HTTPServerResponse & response)
    {
        response.set("Allow", "POST, OPTIONS");
        response.setStatus(Poco::Net::HTTPResponse::HTTP_OK);
    }

    void Authentication::setAuthenticationService(std::shared_ptr<Core::AuthenticationServiceInterface> service)
    {
    	authenticationService = std::move(service);
    }


} }
