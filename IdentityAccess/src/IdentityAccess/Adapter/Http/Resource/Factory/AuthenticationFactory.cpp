// #include "Core/UserAuthenticated.h"
// #include "Adapter/Log/PocoLogger.h"
#include "IdentityAccess/Core/Server.h"
#include "Foundation/Event/EventAggregatorFactory.h"
// #include "Adapter/File/ServerFileRepositoryFactory.h"
#include "IdentityAccess/Adapter/Http/Resource/Authentication.h"
#include "IdentityAccess/Adapter/EPP/EPPAuthenticationService.h"
#include "IdentityAccess/Adapter/Http/Resource/Factory/AuthenticationFactory.h"

namespace Http {
namespace Resource {


    Poco::Net::HTTPRequestHandler * AuthenticationFactory::createResource()
    {
        auto resource             = new Resource::Authentication();
        auto fakeServerObject     = Core::Server(0, "", "", "", "");
        auto eventAggregator      = Foundation::EventAggregatorFactory::create();

        resource->setAuthenticationService(
            std::make_shared<EPP::EPPAuthenticationService>(fakeServerObject, eventAggregator)
        );

        return resource;
    }


} }
