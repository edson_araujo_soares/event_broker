// #include "Adapter/Log/PocoLogger.h"
// #include "Core/SessionInvalidated.h"
#include "Foundation/Event/EventAggregatorFactory.h"
#include "IdentityAccess/Adapter/EPP/EPPSessionService.h"
#include "IdentityAccess/Adapter/Http/Resource/Session.h"
#include "IdentityAccess/Adapter/Http/Resource/Factory/SessionFactory.h"

namespace Http {
namespace Resource {


    Poco::Net::HTTPRequestHandler * SessionFactory::createResource()
    {
        auto resource        = new Resource::Session();
        auto eventAggregator = Foundation::EventAggregatorFactory::create();

        // Log::PocoLogger logging(eventAggregator, Core::SessionInvalidated::className());

        resource->setSessionService(std::make_shared<EPP::EPPSessionService>(eventAggregator));
        return resource;
    }


} }
