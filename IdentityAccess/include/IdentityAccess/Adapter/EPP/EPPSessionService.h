#ifndef EPP_SessionService_INCLUDED
#define EPP_SessionService_INCLUDED

#include <memory>
#include <string>
#include "Foundation/Event/EventAggregatorInterface.h"
#include "IdentityAccess/Core/SessionServiceInterface.h"

namespace EPP {


    class EPPSessionService : public Core::SessionServiceInterface
    {
    public:
        explicit EPPSessionService(std::shared_ptr<Foundation::EventAggregatorInterface>);
        ~EPPSessionService() override = default;
        void invalidate() override;

    private:
        std::shared_ptr<Foundation::EventAggregatorInterface> _eventAggregator;

    };


}

#endif
