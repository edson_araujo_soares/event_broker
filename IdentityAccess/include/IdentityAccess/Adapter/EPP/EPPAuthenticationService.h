#ifndef EPP_EPPAuthenticationService_INCLUDED
#define EPP_EPPAuthenticationService_INCLUDED

#include <memory>
#include <string>
#include "IdentityAccess/Core/Server.h"
// #include "Foundation/EPP/EPPHandlerInterface.h"
#include "Foundation/Event/EventAggregatorInterface.h"
#include "IdentityAccess/Core/AuthenticationServiceInterface.h"

namespace EPP {


    class EPPAuthenticationService : public Core::AuthenticationServiceInterface
    {
    public:
        ~EPPAuthenticationService() override = default;
        EPPAuthenticationService(const Core::Server &, std::shared_ptr<Foundation::EventAggregatorInterface>);
        void authenticate(const std::string & provider, const std::string & password, const std::string & language) override;

    private:
        Core::Server _server;
        std::shared_ptr<Foundation::EventAggregatorInterface> _eventAggregator;

    };


}

#endif
