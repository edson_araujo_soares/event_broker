#ifndef Http_Resource_Authentication_INCLUDED
#define Http_Resource_Authentication_INCLUDED

#include <memory>

#include "Foundation/Http/AbstractResource.h"
#include "IdentityAccess/Core/AuthenticationServiceInterface.h"

namespace Http {
namespace Resource {


    class Authentication : public Foundation::AbstractResource
    {
    public:
        void setAuthenticationService(std::shared_ptr<Core::AuthenticationServiceInterface>);

    protected:
        void handle_post(   Poco::Net::HTTPServerRequest &, Poco::Net::HTTPServerResponse &) override;
        void handle_options(Poco::Net::HTTPServerRequest &, Poco::Net::HTTPServerResponse &) override;

    private:
        std::shared_ptr<Core::AuthenticationServiceInterface> authenticationService;

    };


} }

#endif
