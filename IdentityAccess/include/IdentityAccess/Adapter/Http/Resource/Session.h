#ifndef Http_Resource_Session_INCLUDED
#define Http_Resource_Session_INCLUDED

#include <memory>

#include "Foundation/Http/AbstractResource.h"
#include "IdentityAccess/Core/SessionServiceInterface.h"

namespace Http {
namespace Resource {


    class Session : public Foundation::AbstractResource
    {
    public:
        void setSessionService(std::shared_ptr<Core::SessionServiceInterface>);

    protected:
        void handle_delete( Poco::Net::HTTPServerRequest &, Poco::Net::HTTPServerResponse &) override;
        void handle_options(Poco::Net::HTTPServerRequest &, Poco::Net::HTTPServerResponse &) override;

    private:
        std::shared_ptr<Core::SessionServiceInterface> sessionService;

    };


} }

#endif
