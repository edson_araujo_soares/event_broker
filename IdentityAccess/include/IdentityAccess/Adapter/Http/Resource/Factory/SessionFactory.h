#ifndef Http_Resource_Factory_SessionFactory_INCLUDED
#define Http_Resource_Factory_SessionFactory_INCLUDED

#include "Foundation/Http/ResourceFactoryInterface.h"

namespace Http {
namespace Resource {


    class SessionFactory : public Foundation::ResourceFactoryInterface
    {
    public:
        ~SessionFactory() override = default;
        Poco::Net::HTTPRequestHandler * createResource() override;

    };


} }

#endif
