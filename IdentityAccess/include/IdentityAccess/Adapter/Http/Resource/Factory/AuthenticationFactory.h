#ifndef Http_Resource_Factory_AuthenticationFactory_INCLUDED
#define Http_Resource_Factory_AuthenticationFactory_INCLUDED

#include "Foundation/Http/ResourceFactoryInterface.h"

namespace Http {
namespace Resource {


    class AuthenticationFactory : public Foundation::ResourceFactoryInterface
    {
    public:
        ~AuthenticationFactory() override = default;
        Poco::Net::HTTPRequestHandler * createResource() override;

    };


} }

#endif
