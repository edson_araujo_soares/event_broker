#ifndef IdentityAccess_Core_Server_INCLUDED
#define IdentityAccess_Core_Server_INCLUDED

#include <string>

namespace Core {


    class Server
    {
    public:
        Server() = delete;
        Server(
            int port,
            const std::string & url,
            const std::string & passphrase,
            const std::string & rootCertificate,
            const std::string & clientCertificate
        );

        int port();
        std::string url();
        std::string passphrase();
        std::string rootCertificate();
        std::string clientCertificate();

    private:
        int _port;
        std::string _url;
        std::string _passphrase;
        std::string _rootCertificate;
        std::string _clientCertificate;

    };


}

#endif
