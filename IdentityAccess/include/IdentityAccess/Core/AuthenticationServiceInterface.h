#ifndef IdentityAccess_Core_AuthenticationServiceInterface_INCLUDED
#define IdentityAccess_Core_AuthenticationServiceInterface_INCLUDED

#include <string>

namespace Core {


    class AuthenticationServiceInterface
    {
    public:
        virtual ~AuthenticationServiceInterface() = default;
        virtual void authenticate(const std::string & provider, const std::string & password, const std::string & language) = 0;

    };


}

#endif
