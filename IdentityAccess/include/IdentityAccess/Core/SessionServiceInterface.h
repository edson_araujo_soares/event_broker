#ifndef IdentityAccess_Core_SessionServiceInterface_INCLUDED
#define IdentityAccess_Core_SessionServiceInterface_INCLUDED

#include <string>

namespace Core {


    class SessionServiceInterface
    {
    public:
        virtual ~SessionServiceInterface() = default;
        virtual void invalidate() = 0;

    };


}

#endif
